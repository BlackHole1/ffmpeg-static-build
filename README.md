# ffmpeg-static-build

This project is based on the development of [ffmpeg-static](https://github.com/zimbatm/ffmpeg-static/) based on [zimbatm](https://github.com/zimbatm)

This project is built automatically using gitlab's free runner.

Anyone can open the [CI/CD](https://gitlab.com/BlackHole1/ffmpeg-static-build/pipelines) of this project to view the compiled FFmpeg

Please pay attention to the license of [FDK AAC Codec Library](https://android.googlesource.com/platform/external/aac/+/master/NOTICE) when using.


# Thanks

[zimbatm/ffmpeg-static](https://github.com/zimbatm/ffmpeg-static)

[FFmpeg/FFmpeg](https://github.com/FFmpeg/FFmpeg)

[gitlab-org/gitlab-runner](https://gitlab.com/gitlab-org/gitlab-runner)

[gitlab-org/gitlab-ce](https://gitlab.com/gitlab-org/gitlab-ce)